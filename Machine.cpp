#include "Machine.hpp"

uint32_t Machine::clockTick(){
    fetch();
    issue();
    for(auto it = ROB.rbegin(); it != ROB.rend(); it++)
        if(it->updatedTill < cycle)
            switch(it->stage){
                case Stage::EXEC:
                    execute(&*it);
                    it->updatedTill = cycle;
                    break;
                case Stage::MEM:
                    memory(&*it);
                    it->updatedTill = cycle;
                    break;
                case Stage::CDB:
                    cdb(&*it);
                    it->updatedTill = cycle;
                    break;
                case Stage::COMMIT:
                    break;
            }
    if(ROB.size())
        if((ROB.front().updatedTill < cycle) && (ROB.front().stage == Stage::COMMIT)){
            if(!ROB.front().swCommitted)
                commit(&*ROB.begin());
            else{
                auto temp = ROB.front();
                ROB.pop_front();
                firstUncommitted++;

                commit(&temp);
            }
        }
    cycle++;
    return cycle;
}

void Machine::fetch(){
    if(context->getIndex() > 0)
        IQ.push_back(std::make_pair(context->getIndex(), cycle));
    context->incrementIndex();
}

void Machine::issue(){
    if(((ROB.end() - ROB.begin()) >= 6) || (IQ.size() == 0) || (IQ.front().second == cycle))
        return;

    ROB.push_back(ReorderBufferEntry(firstUncommitted+ROB.size(), cycle, context->getInstruction(IQ.front().first), context->getPrediction(IQ.front().first)));
    auto& temp = ROB.back();
    IQ.pop_front();

    if(temp.inst.type == Defines::InstType::BREAK || temp.inst.type == Defines::InstType::NOP){
        temp.stage = Stage::COMMIT;
        return;
    }

    switch(temp.inst.opcode){
        case Defines::OpCode::_SPECIAL:
            switch(temp.inst.function){
                case Defines::Funct::ADD:
                case Defines::Funct::ADDU:
                case Defines::Funct::SLT:
                case Defines::Funct::SLTU:
                case Defines::Funct::SUB:
                case Defines::Funct::SUBU:
                case Defines::Funct::AND:
                case Defines::Funct::NOR:
                case Defines::Funct::OR:
                case Defines::Funct::XOR:
                    setJ(temp, temp.inst.rs);
                    setK(temp, temp.inst.rt);
                    setDestination(temp, temp.inst.rd);
                    break;
                case Defines::Funct::SLL:
                case Defines::Funct::SRA:
                case Defines::Funct::SRL:
                    setJ(temp, temp.inst.rt);
                    temp.Vk = temp.inst.sa;
                    setDestination(temp, temp.inst.rd);
                    break;
                default:
                    break;
            }
            break;
        case Defines::OpCode::_REGIMM:
        case Defines::OpCode::BGTZ:
        case Defines::OpCode::BLEZ:
            setJ(temp, temp.inst.rs);
            temp.jumpTarget = temp.offset + (temp.inst.imm*4) + 4;
            break;
        case Defines::OpCode::ADDI:
        case Defines::OpCode::ADDIU:
        case Defines::OpCode::SLTI:
            setJ(temp, temp.inst.rs);
            temp.Vk = temp.inst.imm;
            setDestination(temp, temp.inst.rt);
            break;
        case Defines::OpCode::BEQ:
        case Defines::OpCode::BNE:
            setJ(temp, temp.inst.rs);
            setK(temp, temp.inst.rt);
            temp.jumpTarget = temp.offset + (temp.inst.imm*4) + 4;
            break;
        case Defines::OpCode::SW:
            setJ(temp, temp.inst.rt);
            setK(temp, temp.inst.rs);
            temp.memoryAddress = temp.inst.imm;
            //temp.swCommitted = false;
            break;
        case Defines::OpCode::LW:
            setK(temp, temp.inst.rs);
            temp.memoryAddress = temp.inst.imm;
            setDestination(temp, temp.inst.rt);
            break;
        case Defines::OpCode::J:
            temp.jumpTarget = temp.inst.addr*4;
            break;
        default:
            break;
    }
}

void Machine::execute(ReorderBufferEntry* rob){
    if(rob->Qj != -1 || rob->Qk != -1)
        return;

    switch(rob->inst.opcode){
        case Defines::OpCode::_SPECIAL:
            switch(rob->inst.function){
                case Defines::Funct::ADD:
                case Defines::Funct::ADDU:
                    rob->value = rob->Vj + rob->Vk;
                    break;
                case Defines::Funct::SLT:
                    rob->value = (rob->Vj<rob->Vk)?1:0;
                    break;
                case Defines::Funct::SLTU:
                    rob->value = (((uint32_t)(rob->Vj))<((uint32_t)(rob->Vk)))?1:0;
                    break;
                case Defines::Funct::SUB:
                case Defines::Funct::SUBU:
                    rob->value = rob->Vj - rob->Vk;
                    break;
                case Defines::Funct::AND:
                    rob->value = (((uint32_t)(rob->Vj))&((uint32_t)(rob->Vk)));
                    break;
                case Defines::Funct::NOR:
                    rob->value = ~(((uint32_t)(rob->Vj))|((uint32_t)(rob->Vk)));
                    break;
                case Defines::Funct::OR:
                    rob->value = (((uint32_t)(rob->Vj))|((uint32_t)(rob->Vk)));
                    break;
                case Defines::Funct::XOR:
                    rob->value = (((uint32_t)(rob->Vj))^((uint32_t)(rob->Vk)));
                    break;
                case Defines::Funct::SLL:
                    rob->value = ((uint32_t)(rob->Vj))<<rob->Vk;
                    break;
                case Defines::Funct::SRA:
                    if(rob->Vj < 0)
                        rob->value = -1 * (( -1*rob->Vj)>>rob->Vk);
                    else
                        rob->value = rob->Vj >> rob->Vk;
                    break;
                case Defines::Funct::SRL:
                    rob->value = ((uint32_t)(rob->Vj))>>rob->Vk;
                    break;
                default:
                    break;
            } break;
        case Defines::OpCode::_REGIMM:
            switch(rob->inst.functionrt){
                case Defines::RT::BGEZ:
                    rob->branchTaken = (rob->Vj >= 0);
                    break;
                case Defines::RT::BLTZ:
                    rob->branchTaken = (rob->Vj < 0);
                    break;
                default:
                    break;
            } break;
        case Defines::OpCode::BGTZ:
            rob->branchTaken = (rob->Vj > 0);
            break;
        case Defines::OpCode::BLEZ:
            rob->branchTaken = (rob->Vj <= 0);
            break;
        case Defines::OpCode::ADDI:
        case Defines::OpCode::ADDIU:
            rob->value = rob->Vj + rob->Vk;
            break;
        case Defines::OpCode::SLTI:
            rob->value = (rob->Vj<rob->Vk)?1:0;
            break;
        case Defines::OpCode::BEQ:
            rob->branchTaken = (rob->Vj == rob->Vk);
            break;
        case Defines::OpCode::BNE:
            rob->branchTaken = (rob->Vj != rob->Vk);
            break;
        case Defines::OpCode::SW:
        case Defines::OpCode::LW:
            rob->memoryAddress += rob->Vk;
            break;
        case Defines::OpCode::J:
            rob->branchTaken = true;
            break;
        default:
            break;
    }

    if(rob->inst.isBranch() && rob->inst.opcode != Defines::OpCode::J)
        context->recordBTB(rob->offset, rob->jumpTarget, rob->branchTaken);

    if(rob->inst.isBranch() || rob->inst.opcode == Defines::OpCode::SW)
        rob->stage = Stage::COMMIT;
    else if(rob->inst.opcode == Defines::OpCode::LW)
        rob->stage = Stage::MEM;
    else
        rob->stage = Stage::CDB;
}


void Machine::memory(ReorderBufferEntry* rob){
    for(auto it = ROB.begin(); it->ROBIndex != rob->ROBIndex; it++)
        if(it->inst.opcode == Defines::OpCode::SW)
            if(it->Qk > -1 || it->memoryAddress == rob->memoryAddress)
                return;
    rob->value = context->getMemory(rob->memoryAddress);
    rob->stage = Stage::CDB;
}

void Machine::cdb(ReorderBufferEntry* rob){
    for(auto it = ROB.begin(); ROB.end() != it; it++){
        if(it->Qj == rob->ROBIndex){
            it->Vj = rob->value;
            it->Qj = -1;
        }
        if(it->Qk == rob->ROBIndex){
            it->Vk = rob->value;
            it->Qk = -1;
        }
    }
    rob->stage = Stage::COMMIT;
}

void Machine::commit(ReorderBufferEntry* rob){
    if(rob->inst.opcode == Defines::OpCode::J)
        context->recordBTB(rob->offset, rob->jumpTarget, true);
    if(rob->inst.isBranch()){
        if(rob->branchTaken != rob->branchPredicted){
            ROB.clear();
            IQ.clear();
            for(auto it = Registers.begin(); it != Registers.end(); it++)
                it->busy = false;
            context->setIndex(rob->jumpTarget);
        }
    } else if(rob->inst.opcode == Defines::OpCode::SW){
        context->getMemory(rob->memoryAddress) = rob->Vj;
        rob->swCommitted = true;
    }
    else if(rob->inst.isExecutable()) {
        Registers.at(rob->destRegister).value = rob->value;
        if(Registers.at(rob->destRegister).robSource == rob->ROBIndex)
            Registers.at(rob->destRegister).busy = false;
    }

    if(rob->inst.type == Defines::InstType::BREAK)
        running = false;
}


void Machine::setDestination(ReorderBufferEntry& rob, uint32_t regID){
    Registers.at(regID).robSource = rob.ROBIndex;
    Registers.at(regID).busy = true;
    rob.destRegister = regID;
}

ReorderBufferEntry& Machine::getROB(int32_t ROBIndex){
    return *(ROB.begin() + (ROBIndex - firstUncommitted));
}

void Machine::setJ(ReorderBufferEntry& rob, uint32_t regID){
    if(!Registers.at(regID).busy)
        rob.Vj = Registers.at(regID).value;
    else{
        auto& temp = getROB(Registers.at(regID).robSource);
        if(temp.stage == Stage::COMMIT)
            rob.Vj = temp.value;
        else
            rob.Qj = temp.ROBIndex;
    }
}

void Machine::setK(ReorderBufferEntry& rob, uint32_t regID){
    if(!Registers.at(regID).busy)
        rob.Vk = Registers.at(regID).value;
    else{
        auto& temp = getROB(Registers.at(regID).robSource);
        if(temp.stage == Stage::COMMIT)
            rob.Vk = temp.value;
        else
            rob.Qk = temp.ROBIndex;
    }
}
