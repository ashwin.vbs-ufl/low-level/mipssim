#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>

#include "Machine.hpp"

bool processLimits(std::string arg, std::pair<int, int>& limits){
    if(arg.find("-T") != 0)
        return false;
    std::string temp = std::string(arg.begin() + 2, arg.end());
    auto it = temp.find(":");
    if(it == std::string::npos)
        return false;
    std::stringstream(std::string(temp.begin(), temp.begin() + it)) >> limits.first;
    std::stringstream(std::string(temp.begin() + it + 1, temp.end())) >> limits.second;
    return true;
}

bool processOperation(std::string arg, std::string& operation){
    if(arg == "dis" || arg == "sim"){
        operation = arg;
        return true;
    }
    else
        return false;
}

int main(int argc, const char* argv[]){
    std::string inputfile, outputfile, operation;
    std::pair<int, int> limits;

    if(argc != 3 && argc != 4 && argc != 5){
        std::cout<<"Usage: MIPSsim inputfilename outputfilename [operation] [-Tm:n]\n";
        return 1;
    }
    inputfile = std::string(argv[1]);
    outputfile = std::string(argv[2]);
    operation = "sim";
    limits = std::make_pair(-1, -1);

    if(argc == 4){
        if(!processOperation(std::string(argv[3]), operation) && !processLimits(std::string(argv[3]), limits)){
                std::cout<<"Usage: MIPSsim inputfilename outputfilename [operation] [-Tm:n]\n";
                return 1;
            }
    }
    else if(argc == 5){
        if(!(processOperation(std::string(argv[3]), operation) && processLimits(std::string(argv[4]), limits)))
            if(!(processOperation(std::string(argv[4]), operation) && processLimits(std::string(argv[3]), limits))){
                    std::cout<<"Usage: MIPSsim inputfilename outputfilename [operation] [-Tm:n]\n";
                    return 1;
                }
    }

    std::ifstream input(inputfile, std::ios::binary);
    Program program(input);
    input.close();
    std::ofstream output(outputfile, std::ios::trunc);

    if(operation == "dis")
        program.prettyPrintProgram(output);
    else{
        Machine mach;
        ExecutionContext context(&program, 600);
        mach.setContext(&context);
        while(mach.isRunning()){
            int step = mach.clockTick();
            if(step >= limits.first && (step <= limits.second || limits.second == -1))
                output<<mach;
        }
        if(limits.first == 0 &&  limits.second == 0)
            output<<mach;
    }


    output.close();
    return 0;
}
