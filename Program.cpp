#include "Program.hpp"

void swapEndian(uint32_t& number){
    uint8_t first = (number & 0xff<<24)>>24;
    uint8_t second = (number & 0xff<<16)>>16;
    uint8_t third = (number & 0xff<<8)>>8;
    uint8_t fourth = number & 0xff;

    number = first + (second << 8) + (third << 16) + (fourth << 24);
}

Program::Program(std::istream& input){
    uint32_t offset = 600;

    uint32_t inst;
    while(input.read((char *)&inst, sizeof(inst)) && offset < 716){
        swapEndian(inst);
        instructions[offset] = Instruction(offset, inst);
        offset += 4;
        if(inst == 13) break;
    }

    while(offset < 716 && input.read((char *)&inst, sizeof(inst))){
        offset += 4;
    }

    while(input.read((char *)&inst, sizeof(inst))){
        swapEndian(inst);
        memory[offset] = inst;
        offset += 4;
    }
}


ExecutionContext::ExecutionContext(Program* prog, int32_t index): program(prog), BTB(16), currentIndex(index) {}

Instruction& ExecutionContext::getInstruction(int32_t offset){return program->instructions.at(offset);}

int32_t& ExecutionContext::getMemory(int32_t offset){return program->memory.at(offset);}

int32_t ExecutionContext::getIndex(){ return currentIndex; }

void ExecutionContext::incrementIndex(){
    if(currentIndex == -1)
        return;
    else if(getInstruction(currentIndex).instruction == 13)
        currentIndex = -1;
    else
        currentIndex = BTB.getTarget(currentIndex);
}

bool ExecutionContext::getPrediction(int32_t offset){
    return BTB.getPrediction(offset);
}

void ExecutionContext::recordBTB(int32_t offset, int32_t target, bool taken){
    BTB.record(offset, target, taken);
}




void ExecutionContext::printBTB(std::ostream& out){
    out<< BTB;
}

void ExecutionContext::printDataSegment(std::ostream& out){
    program->printDataSegment(out);
}

void Program::printDataSegment(std::ostream& out){
    out<<"716:";
    for(auto it = memory.begin(); it != memory.end(); it++)
        out<<'\t'<<it->second;
}

void printRaw(std::ostream &output, uint32_t number, uint8_t digits){
    uint32_t num = number%2;
    if(digits > 1)
        printRaw(output, number/2, digits-1);
    output<<num;
}

void Program::prettyPrintProgram(std::ostream& out){
    for(auto it = instructions.begin(); it!= instructions.end(); it++){
        	printRaw(out, it->second.instruction>>26, 6);
        	out<<" ";
        	printRaw(out, it->second.instruction>>21, 5);
        	out<<" ";
        	printRaw(out, it->second.instruction>>16, 5);
        	out<<" ";
        	printRaw(out, it->second.instruction>>11, 5);
        	out<<" ";
        	printRaw(out, it->second.instruction>>6, 5);
        	out<<" ";
        	printRaw(out, it->second.instruction, 6);
        	out<<" "<<it->first<<" "<<it->second<<std::endl;
    }
    auto off = instructions.rbegin()->first + 4;
    while(off < 716){
        printRaw(out, 0, 32);
        out<<" "<<off<<" "<<0<<std::endl;
        off += 4;
    }
    for(auto it = memory.begin(); it!= memory.end(); it++){
        	printRaw(out, it->second, 32);
        	out<<" "<<it->first<<" "<<it->second<<std::endl;
    }
}
