#include <iostream>
#include <map>
#include "Instruction.hpp"
#include "BTB.hpp"

class Program{
private:
    std::map<int32_t, Instruction> instructions;
    std::map<int32_t, int32_t> memory;
public:
    Program(std::istream& filename);
    friend class ExecutionContext;
    void printDataSegment(std::ostream& out);
    void prettyPrintProgram(std::ostream& out);
};

class ExecutionContext{
private:
    Program* program;
    BranchTargetBuffer BTB;
    int32_t currentIndex;
public:
    ExecutionContext(Program* prog, int32_t index);

    Instruction& getInstruction(int32_t offset);
    int32_t& getMemory(int32_t offset);

    int32_t getIndex();
    void incrementIndex();

    bool getPrediction(int32_t);
    void recordBTB(int32_t offset, int32_t target, bool taken);
    void setIndex(int32_t index){currentIndex = index;}

    void printBTB(std::ostream& out);
    void printDataSegment(std::ostream& out);
};
