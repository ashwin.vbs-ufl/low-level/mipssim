#include <ostream>
#include "Defines.hpp"

struct Instruction{
    int32_t offset;
    uint32_t instruction;

    Defines::InstType type;

    Defines::OpCode opcode;
    Defines::Funct function;
    Defines::RT functionrt;

    int8_t rs;
    int8_t rt;
    int8_t rd;
    int8_t sa;
    int16_t imm;
    int32_t addr;

    Instruction(int32_t = 0, uint32_t = 0);
    friend std::ostream& operator<<(std::ostream&, Instruction&);

    bool isBranch();
    bool isExecutable();
};
