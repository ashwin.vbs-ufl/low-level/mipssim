#include <cstdint>
#include "Program.hpp"
#include "Register.hpp"

enum class Stage : uint8_t {
    EXEC,
    MEM,
    CDB,
    COMMIT
};

struct ReorderBufferEntry {
    int32_t ROBIndex;
    Stage stage;
    uint32_t updatedTill;
    bool swCommitted;

    Instruction& inst;

    int32_t offset;

    //to be updated during clockticks
    int32_t jumpTarget;

    uint8_t destRegister;
    uint32_t memoryAddress;

    int32_t value;
    int32_t Vj, Vk;
    int32_t Qj, Qk;

    bool branchPredicted;
    bool branchTaken;

    ReorderBufferEntry(int32_t robid, uint32_t clockT, Instruction& instruct, bool branchPrediction):
        ROBIndex(robid),
        stage(Stage::EXEC),
        updatedTill(clockT),
        swCommitted(true),
        inst(instruct),
        offset(instruct.offset),
        jumpTarget(0),
        destRegister(0),
        memoryAddress(0),
        value(0),
        Vj(0),
        Vk(0),
        Qj(-1),
        Qk(-1),
        branchPredicted(branchPrediction),
        branchTaken(false)
    {}
};
