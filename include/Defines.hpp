#include <cstdint>
#include <string>

class Defines{
public:
    enum class InstType : uint8_t {
        RTYPE,
        ITYPE,
        JTYPE,
        NOP,
        BREAK
    };

    enum class OpCode : uint8_t {
        _SPECIAL	= 0,
        _REGIMM		= 1,
        J			= 2,
        JAL			= 3,
        BEQ			= 4,
        BNE			= 5,
        BLEZ		= 6,
        BGTZ		= 7,
        ADDI		= 8,
        ADDIU		= 9,
        SLTI		= 10,
        SLTIU		= 11,
        ANDI		= 12,
        ORI			= 13,
        XORI		= 14,
        LUI			= 15,
        BEQL		= 20,
        BNEL		= 21,
        BLEZL		= 22,
        BGTZL		= 23,
        LB			= 32,
        LH			= 33,
        LWL			= 34,
        LW			= 35,
        LBU			= 36,
        LHU			= 37,
        LWR			= 38,
        SB			= 40,
        SH			= 41,
        SWL			= 42,
        SW			= 43,
        SWR			= 46,
        CACHE		= 47,
        LL			= 48,
        LWC1		= 49,
        LWC2		= 50,
        PREF		= 51,
        LDC1		= 53,
        LDC2		= 54,
        SC			= 56,
        SWC1		= 57,
        SWC2		= 58,
        SDC1		= 61,
        SDC2		= 62,
        TOP_BOUND   = 64
    };

    enum class Funct : uint8_t {
        SLL			= 0,
        MOVCI		= 1,
        SRL			= 2,
        SRA			= 3,
        SLLV		= 4,
        SRLV		= 6,
        SRAV		= 7,
        JR			= 8,
        JALR		= 9,
        MOVZ		= 10,
        MOVN		= 11,
        SYSCALL		= 12,
        BREAK		= 13,
        SYNC		= 15,
        MFHI		= 16,
        MTHI		= 17,
        MFLO		= 18,
        MTLO		= 19,
        MULT		= 24,
        MULTU		= 25,
        DIV			= 26,
        DIVU		= 27,
        ADD			= 32,
        ADDU		= 33,
        SUB			= 34,
        SUBU		= 35,
        AND			= 36,
        OR			= 37,
        XOR			= 38,
        NOR			= 39,
        SLT			= 42,
        SLTU		= 43,
        TGE			= 48,
        TGEU		= 49,
        TLT			= 50,
        TLTU		= 51,
        TEQ			= 52,
        TNE			= 54,
        TOP_BOUND   = 64
    };

    enum class RT : uint8_t {
        BLTZ		= 0,
        BGEZ		= 1,
        BLTZL		= 2,
        BGEZL		= 3,
        TGEI		= 8,
        TGEIU		= 9,
        TLTI		= 10,
        TLTIU		= 11,
        TEQI		= 12,
        TNEI		= 14,
        BLTZAL		= 16,
        BGEZAL		= 17,
        BLTZALL		= 18,
        BGEZALL		= 19,
        TOP_BOUND   = 32
    };

    static std::string getName(OpCode);
    static std::string getName(Funct);
    static std::string getName(RT);
};
