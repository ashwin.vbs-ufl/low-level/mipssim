#include <cstdint>
#include <array>

struct Register{
    bool busy;
    int32_t robSource;
    int32_t value;

    Register():busy(false), robSource(0), value(0) {}
};
