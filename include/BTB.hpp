#include <cstdint>
#include <map>

class BranchTargetBuffer{
private:
    int limit;
    std::map<int32_t, std::pair<int32_t, bool>> records;
public:
    BranchTargetBuffer(int lim): limit(lim) {}

    void record(int32_t offset, int32_t target, bool taken){
        records[offset] = std::make_pair(target, taken);
    }

    int32_t getTarget(int32_t offset){
        auto it = records.find(offset);
        if(it == records.end())
            return offset + 4;
        else if(it->second.second)
            return it->second.first;
        else
            return offset + 4;
    }

    bool getPrediction(int32_t offset){
        auto it = records.find(offset);
        if(it == records.end())
            return false;
        else
            return it->second.second;
    }

    friend std::ostream& operator<<(std::ostream& out, BranchTargetBuffer& btb);
};
