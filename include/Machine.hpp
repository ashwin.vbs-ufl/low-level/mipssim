#include <deque>
#include <vector>
#include <map>
#include <array>

#include "ROB.hpp"

class Machine{
private:
    bool running;
    ExecutionContext* context;
    uint32_t cycle;
    uint32_t firstUncommitted;

    std::deque<std::pair<int32_t, uint32_t>> IQ; //push back and pop front
    std::deque<ReorderBufferEntry> ROB; //push back and pop front
    std::array<Register, 32> Registers;
    std::vector<int32_t> Data;

    void fetch();
    void issue();
    void execute(ReorderBufferEntry*);
    void memory(ReorderBufferEntry*);
    void cdb(ReorderBufferEntry*);
    void commit(ReorderBufferEntry*);

    //helpers
    ReorderBufferEntry& getROB(int32_t robid);
    void setDestination(ReorderBufferEntry& rob, uint32_t regID);
    void setJ(ReorderBufferEntry& rob, uint32_t regID);
    void setK(ReorderBufferEntry& rob, uint32_t regID);

public:
    Machine(): running(false), context(nullptr), cycle(0), firstUncommitted(0) {}
    uint32_t clockTick();
    bool isRunning(){return running;}
    void setContext(ExecutionContext* test){context = test; running = true;}
    friend std::ostream& operator<<(std::ostream&, Machine&);
};
