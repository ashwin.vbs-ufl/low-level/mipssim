#include "Machine.hpp"

std::ostream& operator<<(std::ostream& out, Machine& machine){
    out<<"Cycle <"<<machine.cycle<<">:"<<std::endl;

    out<<"IQ:"<<std::endl;
    for(auto it = machine.IQ.begin(); it != machine.IQ.end(); it++)
        out<<"["<<machine.context->getInstruction(it->first)<<"]"<<std::endl;

    out<<"RS:"<<std::endl;
    for(auto it = machine.ROB.begin(); it != machine.ROB.end(); it++)
        if(it->inst.isExecutable())
            out<<"["<<it->inst<<"]"<<std::endl;

    out<<"ROB:"<<std::endl;
    for(auto it = machine.ROB.begin(); it != machine.ROB.end(); it++)
        out<<"["<<it->inst<<"]"<<std::endl;

    out<<"BTB:"<<std::endl;
    machine.context->printBTB(out);

    out<<"Registers:"<<std::endl;
    out<<"R00:";
    for(int it = 0; it < 8; it++)
        out<<'\t'<<machine.Registers[it].value;
    out<<std::endl;
    out<<"R08:";
    for(int it = 8; it < 16; it++)
        out<<'\t'<<machine.Registers[it].value;
    out<<std::endl;
    out<<"R16:";
    for(int it = 16; it < 24; it++)
        out<<'\t'<<machine.Registers[it].value;
    out<<std::endl;
    out<<"R24:";
    for(int it = 24; it < 32; it++)
        out<<'\t'<<machine.Registers[it].value;
    out<<std::endl;

    out<<"Data Segment:"<<std::endl;
    machine.context->printDataSegment(out);
    out<<std::endl;

    return out;
}

//BTB Printer
std::ostream& operator<<(std::ostream& out, BranchTargetBuffer& btb){
    int i = 1;
    for(auto it = btb.records.begin(); it != btb.records.end(); it++, i++)
        out<<"[Entry "<< i <<"]<"
            <<it->first<<","
            <<it->second.first<<","
            <<it->second.second<<">"<<std::endl;
    return out;
}
