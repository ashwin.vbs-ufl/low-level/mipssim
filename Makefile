APP = MIPSSim
OBJS = Defines.o Instruction.o Machine.o MachinePrinter.o Program.o main.o
CC = g++
CPPFLAGS += -Wall -Werror -std=c++11 -g3 -I ./include

$(APP): $(OBJS) $(LIBS)
	$(CC) -o $@ $(OBJS)

%.o: %.cpp
	$(CC) -c $(CPPFLAGS) $*.cpp -o $*.o

clean:
	rm -rf $(OBJS) MIPSSim
