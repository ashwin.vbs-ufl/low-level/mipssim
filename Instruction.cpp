#include "Instruction.hpp"
#include <cstring>

Instruction::Instruction(int32_t off, uint32_t inst):
offset(off),
instruction(inst),
type(Defines::InstType::NOP),
opcode(Defines::OpCode::TOP_BOUND),
function(Defines::Funct::TOP_BOUND),
functionrt(Defines::RT::TOP_BOUND)
{
    opcode = static_cast<Defines::OpCode>(inst>>26);
    if(inst == 0)
        type = Defines::InstType::NOP;
    else if (inst == 13)
        type = Defines::InstType::BREAK;
    else if(opcode == Defines::OpCode::_SPECIAL){
        type = Defines::InstType::RTYPE;
        function = static_cast<Defines::Funct>(inst & 0b111111);
        rd = ((inst & 0b11111<<11)>>11);
        rs = ((inst & 0b11111<<21)>>21);
        rt = ((inst & 0b11111<<16)>>16);
        sa = ((inst & 0b11111<<5)>>5);

    } else if(opcode == Defines::OpCode::_REGIMM){
        type = Defines::InstType::ITYPE;
        functionrt = static_cast<Defines::RT>((inst & 0b11111<<16)>>16);
        rs = ((inst & 0b11111<<21)>>21);
        uint16_t unsignedval = inst & 0xffff;
        std::memcpy(&imm, &unsignedval, sizeof(uint16_t));
    } else if(  opcode == Defines::OpCode::ADDI ||
                opcode == Defines::OpCode::ADDIU ||
                opcode == Defines::OpCode::BEQ ||
                opcode == Defines::OpCode::BGTZ ||
                opcode == Defines::OpCode::BLEZ ||
                opcode == Defines::OpCode::BNE ||
                opcode == Defines::OpCode::SLTI ||
                opcode == Defines::OpCode::SW ||
                opcode == Defines::OpCode::LW){
        type = Defines::InstType::ITYPE;
        rt = ((inst & 0b11111<<16)>>16);
        rs = ((inst & 0b11111<<21)>>21);
        uint16_t unsignedval = inst & 0xffff;
        std::memcpy(&imm, &unsignedval, sizeof(uint16_t));
    } else if(opcode == Defines::OpCode::J){
        type = Defines::InstType::JTYPE;
        addr = (inst & (~(0b111111<<26)));
    }
}

std::ostream& operator<<(std::ostream& out, Instruction& inst){

    if(inst.instruction == 0){
        out<<"NOP";
        return out;
    }

    if(inst.instruction == 13){
        out<<"BREAK";
        return out;
    }

    switch(inst.opcode){
        case Defines::OpCode::_SPECIAL:
            switch(inst.function){
                case Defines::Funct::ADD:
                case Defines::Funct::ADDU:
                case Defines::Funct::AND:
                case Defines::Funct::NOR:
                case Defines::Funct::OR:
                case Defines::Funct::SLT:
                case Defines::Funct::SLTU:
                case Defines::Funct::SUB:
                case Defines::Funct::SUBU:
                case Defines::Funct::XOR:
                    out  <<Defines::getName(inst.function)<<" "
                            <<"R"<< (int)inst.rd << ", "
                            <<"R"<< (int)inst.rs << ", "
                            <<"R"<< (int)inst.rt;
                    break;
                case Defines::Funct::SLL:
                case Defines::Funct::SRA:
                case Defines::Funct::SRL:
                    out  <<Defines::getName(inst.function)<<" "
                            <<"R"<< (int)inst.rd << ", "
                            <<"R"<< (int)inst.rt << ", "
                            <<"#"<< (int)inst.sa;
                    break;
                default:
                    out<<"unsupported function";
                    break;
            }
        break;

        case Defines::OpCode::_REGIMM:{
            if(inst.functionrt == Defines::RT::BGEZ || inst.functionrt == Defines::RT::BLTZ){
                out <<Defines::getName(inst.functionrt)<<" "
                        <<"R"<<(int)inst.rs << ", "
                        <<"#"<<inst.imm*4;
            } else
                out<<"unsupported RT value";
        }break;

        case Defines::OpCode::ADDI:
        case Defines::OpCode::ADDIU:{
            out <<Defines::getName(inst.opcode)<<" "
                    <<"R"<< (int)inst.rt << ", "
                    <<"R"<< (int)inst.rs << ", "
                    <<"#"<< inst.imm;
        }break;

        case Defines::OpCode::BEQ:
        case Defines::OpCode::BNE:{
            out <<Defines::getName(inst.opcode)<<" "
                    <<"R"<< (int)inst.rs << ", "
                    <<"R"<< (int)inst.rt << ", "
                    <<"#"<< inst.imm*4;
        }break;

        case Defines::OpCode::BGTZ:
        case Defines::OpCode::BLEZ:{
            out <<Defines::getName(inst.opcode)<<" "
                    <<"R"<< (int)inst.rs << ", "
                    <<"#"<< inst.imm*4;
        }break;

        case Defines::OpCode::SLTI:{
            out <<Defines::getName(inst.opcode)<<" "
                    <<"R"<< (int)inst.rt << ", "
                    <<"R"<< (int)inst.rs << ", "
                    <<"#"<< inst.imm;
        }break;

        case Defines::OpCode::SW:
        case Defines::OpCode::LW:{
            out <<Defines::getName(inst.opcode)<<" "
                    <<"R"<< (int)inst.rt << ", "
                    <<inst.imm<<"("
                    <<"R"<< (int)inst.rs <<")";
        }break;

        case Defines::OpCode::J:
            out <<Defines::getName(inst.opcode)<<" "
                    <<"#"<<inst.addr*4;
        break;

        default:
            out<<"unsupported op code";
        break;
    }
    return out;
}



bool Instruction::isBranch(){
    switch(opcode){
        case Defines::OpCode::_REGIMM:
        case Defines::OpCode::BGTZ:
        case Defines::OpCode::BLEZ:
        case Defines::OpCode::BEQ:
        case Defines::OpCode::BNE:
        case Defines::OpCode::J:
            return true;
        default:
            return false;
    }
}

bool Instruction::isExecutable(){
    return !(type == Defines::InstType::NOP || type == Defines::InstType::BREAK);
}
