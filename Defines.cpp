#include "Defines.hpp"

std::string Defines::getName(Defines::OpCode code){
    switch(code){
        case OpCode::J: return "J"; break;
        case OpCode::JAL: return "JAL"; break;
        case OpCode::BEQ: return "BEQ"; break;
        case OpCode::BNE: return "BNE"; break;
        case OpCode::BLEZ: return "BLEZ"; break;
        case OpCode::BGTZ: return "BGTZ"; break;
        case OpCode::ADDI: return "ADDI"; break;
        case OpCode::ADDIU: return "ADDIU"; break;
        case OpCode::SLTI: return "SLTI"; break;
        case OpCode::SLTIU: return "SLTIU"; break;
        case OpCode::ANDI: return "ANDI"; break;
        case OpCode::ORI: return "ORI"; break;
        case OpCode::XORI: return "XORI"; break;
        case OpCode::LUI: return "LUI"; break;
        case OpCode::BEQL: return "BEQL"; break;
        case OpCode::BNEL: return "BNEL"; break;
        case OpCode::BLEZL: return "BLEZL"; break;
        case OpCode::BGTZL: return "BGTZL"; break;
        case OpCode::LB: return "LB"; break;
        case OpCode::LH: return "LH"; break;
        case OpCode::LWL: return "LWL"; break;
        case OpCode::LW: return "LW"; break;
        case OpCode::LBU: return "LBU"; break;
        case OpCode::LHU: return "LHU"; break;
        case OpCode::LWR: return "LWR"; break;
        case OpCode::SB: return "SB"; break;
        case OpCode::SH: return "SH"; break;
        case OpCode::SWL: return "SWL"; break;
        case OpCode::SW: return "SW"; break;
        case OpCode::SWR: return "SWR"; break;
        case OpCode::CACHE: return "CACHE"; break;
        case OpCode::LL: return "LL"; break;
        case OpCode::LWC1: return "LWC1"; break;
        case OpCode::LWC2: return "LWC2"; break;
        case OpCode::PREF: return "PREF"; break;
        case OpCode::LDC1: return "LDC1"; break;
        case OpCode::LDC2: return "LDC2"; break;
        case OpCode::SC: return "SC"; break;
        case OpCode::SWC1: return "SWC1"; break;
        case OpCode::SWC2: return "SWC2"; break;
        case OpCode::SDC1: return "SDC1"; break;
        case OpCode::SDC2: return "SDC2"; break;
        case OpCode::TOP_BOUND: return "TOP_BOUND"; break;
        default: return "Unsupported Opcode"; break;
    }
}

std::string Defines::getName(Defines::Funct funct){
    switch(funct){
        case Funct::SLL: return "SLL"; break;
        case Funct::MOVCI: return "MOVCI"; break;
        case Funct::SRL: return "SRL"; break;
        case Funct::SRA: return "SRA"; break;
        case Funct::SLLV: return "SLLV"; break;
        case Funct::SRLV: return "SRLV"; break;
        case Funct::SRAV: return "SRAV"; break;
        case Funct::JR: return "JR"; break;
        case Funct::JALR: return "JALR"; break;
        case Funct::MOVZ: return "MOVZ"; break;
        case Funct::MOVN: return "MOVN"; break;
        case Funct::SYSCALL: return "SYSCALL"; break;
        case Funct::BREAK: return "BREAK"; break;
        case Funct::SYNC: return "SYNC"; break;
        case Funct::MFHI: return "MFHI"; break;
        case Funct::MTHI: return "MTHI"; break;
        case Funct::MFLO: return "MFLO"; break;
        case Funct::MTLO: return "MTLO"; break;
        case Funct::MULT: return "MULT"; break;
        case Funct::MULTU: return "MULTU"; break;
        case Funct::DIV: return "DIV"; break;
        case Funct::DIVU: return "DIVU"; break;
        case Funct::ADD: return "ADD"; break;
        case Funct::ADDU: return "ADDU"; break;
        case Funct::SUB: return "SUB"; break;
        case Funct::SUBU: return "SUBU"; break;
        case Funct::AND: return "AND"; break;
        case Funct::NOR: return "NOR"; break;
        case Funct::SLT: return "SLT"; break;
        case Funct::SLTU: return "SLTU"; break;
        case Funct::TGE: return "TGE"; break;
        case Funct::TGEU: return "TGEU"; break;
        case Funct::TLT: return "TLT"; break;
        case Funct::TLTU: return "TLTU"; break;
        case Funct::TEQ: return "TEQ"; break;
        case Funct::TNE: return "TNE"; break;
        case Funct::TOP_BOUND: return "TOP_BOUND"; break;
        default: return "Unsupported Function"; break;
    }
}

std::string Defines::getName(Defines::RT rt){
    switch(rt){
        case RT::BLTZ: return "BLTZ"; break;
        case RT::BGEZ: return "BGEZ"; break;
        case RT::BLTZL: return "BLTZL"; break;
        case RT::BGEZL: return "BGEZL"; break;
        case RT::TGEI: return "TGEI"; break;
        case RT::TGEIU: return "TGEIU"; break;
        case RT::TLTI: return "TLTI"; break;
        case RT::TLTIU: return "TLTIU"; break;
        case RT::TEQI: return "TEQI"; break;
        case RT::TNEI: return "TNEI"; break;
        case RT::BLTZAL: return "BLTZAL"; break;
        case RT::BGEZAL: return "BGEZAL"; break;
        case RT::BLTZALL: return "BLTZALL"; break;
        case RT::BGEZALL: return "BGEZALL"; break;
        case RT::TOP_BOUND: return "TOP_BOUND"; break;
        default: return "Unsupported RT Value"; break;
    }
}
